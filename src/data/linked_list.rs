use alloc::boxed::Box;

struct KeyValueLinkedListNode<K, V>
where
    K: PartialEq + Sized,
{
    key: K,
    value: V,
    next: Option<Box<KeyValueLinkedListNode<K, V>>>,
}

impl<K, V> KeyValueLinkedListNode<K, V>
where
    K: PartialEq + Sized,
{
    #[inline]
    pub fn new(key: K, value: V) -> Self {
        KeyValueLinkedListNode {
            key,
            value,
            next: None,
        }
    }
}

pub struct KeyValueLinkedList<K, V>
where
    K: PartialEq + Sized,
{
    head: Option<Box<KeyValueLinkedListNode<K, V>>>,
}

impl<K, V> KeyValueLinkedList<K, V>
where
    K: PartialEq + Sized,
{
    #[inline]
    pub fn new() -> Self {
        KeyValueLinkedList { head: None }
    }
}

impl<K, V> KeyValueLinkedList<K, V>
where
    K: PartialEq + Sized,
{
    pub fn search(&self, key: &K) -> Option<&V> {
        self.head.as_deref()?;

        let mut current_node = &self.head;

        loop {
            match current_node {
                None => return None,
                Some(node) if &node.key == key => return Some(&node.value),
                Some(node) => current_node = &node.next,
            }
        }
    }

    pub fn insert(&mut self, key: K, value: V) {
        if self.head.as_deref().is_none() {
            self.head = Some(Box::new(KeyValueLinkedListNode::new(key, value)));
            return;
        }

        let mut current_node = &mut self.head;

        loop {
            match current_node {
                None => (),
                Some(node) if node.key == key => (),
                Some(node) if node.next.is_none() => {
                    node.next = Some(Box::new(KeyValueLinkedListNode::new(key, value)));
                    return;
                }
                Some(node) => current_node = &mut node.next,
            }
        }
    }

    pub fn delete(&mut self, key: &K) {
        let mut current_node = &mut self.head;

        loop {
            match current_node {
                None => break,
                Some(node) if &node.key == key => {
                    *current_node = node.next.take();
                    break;
                }
                Some(node) => current_node = &mut node.next,
            }
        }
    }
}
