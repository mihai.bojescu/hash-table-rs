use alloc::boxed::Box;

use super::linked_list;

pub struct HashTable<K, V>
where
    K: PartialEq + Sized,
{
    hash_function: Box<dyn Fn(&K) -> usize>,
    buckets: [Option<Box<linked_list::KeyValueLinkedList<K, V>>>; 32],
}

impl<K, V> HashTable<K, V>
where
    K: PartialEq + Sized,
{
    #[inline]
    pub fn new(hash_function: Box<dyn Fn(&K) -> usize>) -> Self {
        Self {
            hash_function,
            buckets: Default::default(),
        }
    }
}

impl<K, V> HashTable<K, V>
where
    K: PartialEq + Sized,
{
    fn hash(&self, key: &K) -> usize {
        let hash_function = self.hash_function.as_ref();
        hash_function(key) % 32
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        let hashed_key = self.hash(key);
        let bucket = &self.buckets[hashed_key];

        match bucket {
            Some(list) => list.search(key),
            None => None,
        }
    }

    pub fn insert(&mut self, key: K, value: V) {
        let hashed_key = self.hash(&key);
        let bucket = &mut self.buckets[hashed_key];

        match bucket {
            Some(list) => list.insert(key, value),
            None => {
                let mut list = linked_list::KeyValueLinkedList::new();
                list.insert(key, value);

                self.buckets[hashed_key] = Some(Box::new(list))
            }
        }
    }

    pub fn delete(&mut self, key: &K) {
        let hashed_key = self.hash(key);
        let bucket = &mut self.buckets[hashed_key];

        match bucket {
            Some(list) => list.delete(key),
            None => (),
        }
    }
}

#[cfg(test)]
mod tests {
    use alloc::{
        boxed::Box,
        string::{String, ToString},
    };

    use super::HashTable;

    fn hash(key: &&str) -> usize {
        let mut result = 0;

        for &byte in key.as_bytes() {
            result += byte as usize;
        }

        result
    }

    #[test]
    pub fn construct() {
        HashTable::<&str, usize>::new(Box::new(hash));
    }

    #[test]
    pub fn insert() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value", 0);

        assert_eq!(hash_map.get(&"value"), Some(&0));
    }

    #[test]
    pub fn insert_no_collision() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value1", 1);
        hash_map.insert("value2", 2);

        assert_eq!(hash_map.get(&"value1"), Some(&1));
        assert_eq!(hash_map.get(&"value2"), Some(&2));
    }

    #[test]
    pub fn insert_many_no_collision() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value1", 1);
        hash_map.insert("value2", 2);
        hash_map.insert("value3", 3);
        hash_map.insert("value4", 4);

        assert_eq!(hash_map.get(&"value1"), Some(&1));
        assert_eq!(hash_map.get(&"value2"), Some(&2));
        assert_eq!(hash_map.get(&"value3"), Some(&3));
        assert_eq!(hash_map.get(&"value4"), Some(&4));
    }

    #[test]
    pub fn insert_collision() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value1", 0);
        hash_map.insert("1value", 1);

        assert_eq!(hash_map.get(&"value1"), Some(&0));
        assert_eq!(hash_map.get(&"1value"), Some(&1));
    }

    #[test]
    pub fn insert_many_collision() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value1", 0);
        hash_map.insert("1value", 1);
        hash_map.insert("v1alue", 2);
        hash_map.insert("va1lue", 3);
        hash_map.insert("val1ue", 4);
        hash_map.insert("valu1e", 5);

        assert_eq!(hash_map.get(&"value1"), Some(&0));
        assert_eq!(hash_map.get(&"1value"), Some(&1));
        assert_eq!(hash_map.get(&"v1alue"), Some(&2));
        assert_eq!(hash_map.get(&"va1lue"), Some(&3));
        assert_eq!(hash_map.get(&"val1ue"), Some(&4));
        assert_eq!(hash_map.get(&"valu1e"), Some(&5));
    }

    #[test]
    pub fn insert_many_random() {
        let mut values: [String; 32] = Default::default();
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        for (i, value) in values.iter_mut().enumerate() {
            *value = ((i * 2) % 255).to_string();
        }

        for value in values.iter() {
            hash_map.insert(value, value.parse::<usize>().unwrap());
        }

        for value in values.iter() {
            assert_eq!(
                hash_map.get(&value.as_str()),
                Some(&value.parse::<usize>().unwrap())
            );
        }
    }

    #[test]
    pub fn delete() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value", 0);
        hash_map.delete(&"value");

        assert_eq!(hash_map.get(&"value"), None);
    }

    #[test]
    pub fn delete_empty() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.delete(&"value");

        assert_eq!(hash_map.get(&"value"), None);
    }

    #[test]
    pub fn delete_too_many() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value", 0);
        hash_map.delete(&"value");
        hash_map.delete(&"non_existent_value");

        assert_eq!(hash_map.get(&"value"), None);
    }

    #[test]
    pub fn delete_twice() {
        let mut hash_map = HashTable::<&str, usize>::new(Box::new(hash));

        hash_map.insert("value", 0);
        hash_map.delete(&"value");
        hash_map.delete(&"value");

        assert_eq!(hash_map.get(&"value"), None);
    }
}
